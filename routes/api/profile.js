const express = require("express");
const router = express.Router();

//@route GET api/profile/test
//@desc Tests profile route
//@access Public

//Each route that we create will have header as above, a standard

router.get("/test", (req, res) => res.json({ msg: "Profile works" })); //same as res.send, but send output as json
module.exports = router;
