const express = require("express");
const router = express.Router();

//@route GET api/posts/test
//@desc Tests post route
//@access Public

//Each route that we create will have header as above, a standard

router.get("/test", (req, res) => res.json({ msg: "Posts works" })); //same as res.send, but send output as json
module.exports = router;
